function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
 
}
function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
 
}

function eliminar(){
	var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
	var clave = txtClave.value;
	sessionStorage.removeItem(clave);
	var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "";
}